import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor() {
    }

    ngOnInit() {
        firebase.initializeApp({
             apiKey: 'AIzaSyC1eeYENMBxni5wBjdNtaNX0wbReZ9SIGY',
             authDomain: 'mynew-c9a22.firebaseapp.com'

        });

    }
}
