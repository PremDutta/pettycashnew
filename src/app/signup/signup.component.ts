import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { NgForm } from '@angular/forms';
import {AuthService} from './auth.service';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    constructor( private authService: AuthService) {
    }

   ngOnInit() {


   }

    onSubmit(form: NgForm)
    {
     const name = form.value.name;
     const email = form.value.email;
     const password = form.value.password;
     const cpassword = form.value.cpassword;
     this.authService.signupuser(name, email, password, cpassword);


     }

}
